import axios from "axios";
import React, { Component } from "react";
import Loader from "../loading/Loading";

import "./Show.css";

class Show extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      data: [],
      didReceive: false,
      loading: true,
      error: false,
    };
  }

  async componentDidMount() {
    const result = await axios.get(
      `https://api.tvmaze.com/shows/${this.props.match.params.id}`
    );
    const data = result.data;
    this.setState({
      data,
      didReceive: true,
      loading: false,
      error: false,
    });
  }

  render() {
    const show = this.state.data;
    return (
      <>
        {this.state.loading && <Loader />}
        <div className="wrapper">
          <div className="main_card">
            <div className="card_left">
              <div className="card_datails">
                <h1>{show.name}</h1>
                <div className="card_cat">
                  <p className="year">Premiered: {show.premiered}</p>
                  <p className="genre">{show.genres}</p>
                  <p className="time">Runtime: {show.runtime} hrs</p>
                </div>
                <div
                  className="disc"
                  dangerouslySetInnerHTML={{ __html: show.summary }}
                ></div>
              </div>
            </div>
            <div className="card_right">
              <div className="img_container">
                {this.state.didReceive && (
                  <img src={show.image.medium} alt="" />
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Show;
