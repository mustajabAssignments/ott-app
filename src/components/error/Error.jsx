import React from "react";

import "./Error.css";

export default function Error({ message }) {
  return (
    <div className="parent">
      <div className="box">
        <h1 className="internet">{message}</h1>
        <div className="btn-parent">
          <button className="button3">Try Again</button>
        </div>
      </div>
    </div>
  );
}
