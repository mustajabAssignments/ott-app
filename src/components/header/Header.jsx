import React from "react";
import "./Header.css";

import logo from "./logo.png";
import SearchForm from "../search/SearchForm";

export default function Header() {
  return (
    <div className="header">
      <div className="logo">
        <img src={logo} alt="Brand  Logo" />
        <h1>myOTT</h1>
      </div>

      <SearchForm />
    </div>
  );
}
