import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./SearchForm.css";

const SearchForm = () => {
  const [query, setQuery] = useState("");
  const [results, setResults] = useState([]);

  const handleInputChange = (event) => {
    setQuery(event.target.value);
  };

  const getResults = async () => {
    const response = await axios.get(
      `http://api.tvmaze.com/search/shows?q=${query}`
    );
    setResults(response.data);
  };

  useEffect(() => {
    if (query) {
      getResults();
    }
  }, [query]);

  return (
    <div className="SearchForm">
      <input
        type="text"
        value={query}
        onChange={handleInputChange}
        placeholder="Search for a TV show"
      />
      <div className="SearchForm-results">
        {results.map((result) => (
          <Link
            key={result.show.id}
            to={`/${result.show.id}`}
            className="SearchForm-result"
          >
            <>
              <div className="SearchForm-result-info">
                <p>{result.show.name}</p>
              </div>
            </>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default SearchForm;
