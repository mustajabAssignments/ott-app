import React, { Component } from "react";

import "./Loading.css";

class Loader extends Component {
  state = {};
  render() {
    return (
      <div id="load" className="wrap">
        <div className="lds-ring">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }
}

export default Loader;
