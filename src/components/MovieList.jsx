import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import useStore from "../app/movieStore";
import Error from "./error/Error";

import Loader from "./loading/Loading";

import "./MovieList.css";

function MovieCard({ show }) {
  return (
    <li className="cards_item">
      <div className="card">
        <div className="card_image">
          <img src={show.image.medium} />
        </div>
        <div className="card_content">
          <h2 className="card_title">{show.name}</h2>
          <div className="card_text">
            <p>{show.rating.average} / 10</p>
            <p>{show.genres.join(" . ")}</p>
          </div>
          <button className="btn card_btn read-more-btn">
            <Link to={`/${show.id}`}>Read More</Link>
          </button>
        </div>
      </div>
    </li>
  );
}

const MovieList = () => {
  const { data, error, isLoading, fetchData } = useStore();

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {isLoading && <Loader />}
      {error && <Error message={error.message} />}
      <div className="main">
        <ul className="cards">
          {data.map((show) => (
            <MovieCard show={show} key={show.id} />
          ))}
        </ul>
      </div>
    </>
  );
};

export default MovieList;
