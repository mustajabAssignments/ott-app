import { create } from "zustand";
import axios from "axios";

const useStore = create((set) => ({
  data: [],
  error: null,
  isLoading: false,
  fetchData: async () => {
    set((state) => ({ isLoading: true }));
    try {
      const response = await axios.get("https://api.tvmaze.com/shows");
      const data = response.data;
      set((state) => ({ data, isLoading: false }));
    } catch (error) {
      set((state) => ({ error, isLoading: false }));
    }
  },
}));

export default useStore;
