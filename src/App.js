import React from "react";

import "bootstrap/dist/css/bootstrap.css";

import Header from "./components/header/Header";
import MovieList from "./components/MovieList";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { Switch, Route } from "react-router-dom";
import Show from "./components/show/Show";

function Main() {
  return (
    <div className="container-fluid movie-app">
      <div className="row">
        <MovieList />
      </div>
    </div>
  );
}

export default function App() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/:id" component={Show} />
      </Switch>
    </BrowserRouter>
  );
}
